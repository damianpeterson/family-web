export async function loadMessages() {
  return await new Promise((resolve, reject) => {
    setTimeout(function () {
      let messages = {
        "messages": [
          {
            "id": 1
          }
        ]
      };
      resolve(messages);
    }, 2000);
  });
}

/*
export async function loadMessages() {
  return await fetch('http://www.mocky.io/v2/5af9bbc32e00006000278d65').then((response) => {
    return response.json();
  }).then((data) => {
    return data.messages;
  });
}
*/
