var app = document.getElementById('app');
app.innerHTML = "<p>Sorry, it looks like you're using a browser that doesn't support the technologies on this site.</p>";
app.innerHTML += "<p>Try <a href='https://family-old.peterson.nz/'>this site</a> instead.</p>";
