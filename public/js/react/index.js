import Messages from './components/messages.js';
import Users from './components/users.js';

const e = React.createElement;

ReactDOM.render([
  e(Users, {}),
  e(Messages, {
    onChange: function (value) {
      console.log(value);
    }
  }),
], document.getElementById('app'));
