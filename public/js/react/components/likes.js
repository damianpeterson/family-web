const e = React.createElement;

class Likes extends React.Component {
  render() {
    let isOwnMessage = this.props.message.user.id === this.props.user.id;

    return e(
      'div',
      {
        className: `likes${isOwnMessage ? ' likes-self' : ''}`,
        onClick: function (event) {
          if (isOwnMessage === false) {
            console.log('clicked');
          }
        }
      },
      this.props.message.likes
    );
  }
}

/*
<div class="likes">
  <a href="/message/19828/like" class="like" data-toggle="tooltip" title="" data-trigger="hover" data-placement="top" data-original-title="Like this">
    <span class="glyphicon glyphicon-thumbs-up"></span> <span class="like-count"></span>
  </a>
</div>
*/

Likes.propTypes = {
  message: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

export default Likes;
