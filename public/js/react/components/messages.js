import Message from './message.js';

const e = React.createElement;

class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.updateInput = this.updateInput.bind(this);
    this.state = {
      messages: []
    };
  }

  async getMessages() {
    let messages = await fetch('http://www.mocky.io/v2/5af9bbc32e00006000278d65')
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        return data.messages;
      });
    this.setState({
      messages: messages
    });
  }

  componentDidMount() {
    this.getMessages();
  }

  componentWillUnmount() {
    console.log('unmount');
  }

  updateInput(event) {
    this.props.onChange(event.target.value);
  }

  render() {
    let messages = this.state.messages.map((message) =>
      e(Message, {message: message, key: message.id, user: {id: 3}})
    );

    let dummyInput = e(
      'input',
      {
        type: 'text',
        onInput: this.updateInput
      }
    );

    return e('div', {className: 'messages'}, [
      messages,
      // dummyInput
    ]);
  }
}

export default Messages;
