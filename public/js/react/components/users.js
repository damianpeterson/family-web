import User from './user.js';

const e = React.createElement;

export default class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  async getUsers() {
    let users = await fetch('http://www.mocky.io/v2/5b0e486e3200006d00c1995b')
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        data.users.forEach((user) => {
          user.isActive = false;
        });
        return data.users;
      });
    this.setState({
      users: users
    });
  }

  componentDidMount() {
    this.getUsers();
  }

  render() {
    let users = this.state.users.map((user) =>
      e(User, {user: user, key: user.id, currentUser: {id: 3}})
    );

    return e('div', {className: 'users'}, [
      users
    ]);
  }
}
