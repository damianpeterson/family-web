import Likes from './likes.js';

const e = React.createElement;

class Message extends React.Component {
  render() {
    return e('div', {className: `message user-${this.props.message.user.id}`},
      this.props.message.photo ? e('img', {
        className: 'photo',
        src: this.props.message.photo
      }) : null,
      this.props.message.body ? e('div', {className: 'body'},
        this.props.message.body
      ) : null,
      e('div', {className: 'details'},
        e('div', {className: 'user'}, this.props.message.user.name),
        e('div', {className: 'created-at'}, this.props.message.createdAt),
        e(Likes, {message: this.props.message, user: this.props.user}),
      ),
    );
  }
}

Message.propTypes = {
  message: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

export default Message;
