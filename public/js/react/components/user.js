const e = React.createElement;

export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: props.currentUser,
      user: props.user,
    };
  }

  render() {
    return e('div', {className: `user user-${this.state.user.id}${this.state.user.isActive ? ' is-active' : ''}`},
      [
        e('div', {
            className: 'heading',
            onClick: () => {
              this.setState({
                user: {
                  ...this.state.user,
                  isActive: !this.state.user.isActive
                }
              });
            }
          },
          [
            e('div', {className: 'name'}, this.state.user.name),
            e('div', {className: 'birthday'}, this.state.user.birthday),
          ]
        ),
        e('div', {className: 'details'},
          [
            this.state.user.address ? e('dl', {}, [
              e('dt', {}, 'Address'),
              e('dd', {}, this.state.user.address),
            ]) : null,
            this.state.user.email ? e('dl', {}, [
              e('dt', {}, 'Email'),
              e('dd', {}, [
                e('a', {href: `mailto:${this.state.user.email}`}, this.state.user.email)
              ]),
            ]) : null,
            this.state.user.mobile ? e('dl', {}, [
              e('dt', {}, 'Mobile'),
              e('dd', {}, [
                e('a', {href: `tel:${this.state.user.mobile}`}, this.state.user.mobile)
              ]),
            ]) : null,
            this.state.currentUser.id === this.state.user.id ? e('a', {className: 'edit-details'}, 'Edit my details') : null,
          ]
        )
      ]
    );
  }
}

User.propTypes = {
  currentUser: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};
