/**
 * Create an element with attributes and contents
 * Supply the name of the element i.e. 'div' or 'input', etc
 * Add any element attributes ('className', not 'class'). 
 * Add event listeners within an 'on' attribute.
 * Any other content should be supplied as an array.
 * 
 * Example:
 * e('div', { className: 'form-control' }, [
 *   'Hello ',
 *   e('input', {
 *     type: 'checkbox',
 *     on: {
 *       change: function (event) {
 *         console.log('changed');
 *       }
 *     }
 *   }),
 *   ' world'
 * ]);
 * 
 * Outputs: <div class="form-control">Hello <input type="checkbox"> world</div>
 *
 * @param element
 * @param attributes
 * @param contents
 * @returns {HTMLElement}
 */
export function e(element, attributes = {}, contents = []) {
  let newElement = document.createElement(element);

  // attach attributes and event listeners
  Object.keys(attributes).forEach((key) => {
    if (key === 'on') {
      Object.keys(attributes[key]).forEach((eventListenerType) => {
        newElement.addEventListener(eventListenerType, 
          attributes[key][eventListenerType]);
      });
    } else {
      newElement[key] = attributes[key];
    }
  });

  // make sure contents are in array
  if (Array.isArray(contents) !== true && contents !== undefined) {
    contents = [contents];
  }

  // append contents
  contents.forEach((content) => {
    if (typeof content === 'object' && content instanceof Element) {
      newElement.appendChild(content);
    } else if (typeof content === 'string') {
      let textElement = document.createTextNode(content);
      newElement.appendChild(textElement);
    }
  });

  return newElement;
}
