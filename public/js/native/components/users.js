import { e } from '../element.js';
import User from './user.js';

export default class Users {
  constructor(currentUser) {
    this.currentUser = currentUser;
    this.users = [];
  }

  async loadUsers(usersWrapper) {
    this.users = await fetch('http://www.mocky.io/v2/5b0e486e3200006d00c1995b').then((response) => {
      return response.json();
    }).then((data) => {
      return data.users;
    });

    this.users.forEach((user) => {
      let userElement = new User(this.currentUser, user);
      usersWrapper.appendChild(userElement.render());
    });
  }

  render() {
    let usersWrapper = e('div', {className: 'users'}, [
      this.users
    ]);

    this.loadUsers(usersWrapper);

    return usersWrapper;
  }
}

