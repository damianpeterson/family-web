import { e } from '../element.js';
import Likes from './likes.js';

export default class Message {
  constructor(currentUser, message) {
    this.currentUser = currentUser;
    this.message = message;
  }

  render() {
    let likes = new Likes(this.message, this.currentUser);
    return e('div', {className: `message user-${this.message.user.id}`}, [
      this.message.photo ? e('img', {
        className: 'photo',
        src: this.message.photo
      }) : null,
      this.message.body ? e('div', {className: 'body'}, this.message.body) : null,
      e('div', {className: 'details'}, [
        e('div', {className: 'user'}, this.message.user.name),
        e('div', {className: 'created-at'}, this.message.createdAt),
        likes.render()
      ]),
    ]);
  }
}
