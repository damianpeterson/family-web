import {e} from '../element.js';

export default class User {
  constructor(currentUser, user) {
    this.currentUser = currentUser;
    this.user = user;
  }

  render() {
    return e('div', {className: `user user-${this.user.id}${this.user.isActive ? ' is-active' : ''}`}, [
      e('div', {
        className: 'heading',
        on: {
          click: function () {
            this.parentNode.classList.toggle('is-active');
          }
        }
      }, [
        e('div', {class: 'name'}, this.user.name),
        e('div', {class: 'birthday'}, this.user.birthday),
      ]),
      e('div', {className: 'details'},
        [
          this.user.address ? e('dl', {}, [
            e('dt', {}, 'Address'),
            e('dd', {}, this.user.address),
          ]) : null,
          this.user.email ? e('dl', {}, [
            e('dt', {}, 'Email'),
            e('dd', {}, [
              e('a', {href: `mailto:${this.user.email}`}, this.user.email)
            ]),
          ]) : null,
          this.user.mobile ? e('dl', {}, [
            e('dt', {}, 'Mobile'),
            e('dd', {}, [
              e('a', {href: `tel:${this.user.mobile}`}, this.user.mobile)
            ]),
          ]) : null,
          this.currentUser.id === this.user.id ? e('a', {className: 'edit-details'}, 'Edit my details') : null,
        ]
      )
    ]);
  }
}
