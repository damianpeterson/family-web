import { e } from '../element.js';
import Message from './message.js';

export default class Messages {
  constructor(currentUser) {
    this.currentUser = currentUser;
    this.messages = [];
  }

  async loadMessages(messagesWrapper) {
    this.messages = await fetch('http://www.mocky.io/v2/5af9bbc32e00006000278d65').then((response) => {
      return response.json();
    }).then((data) => {
      return data.messages;
    });

    this.messages.forEach((message) => {
      let messageElement = new Message(this.currentUser, message);
      messagesWrapper.appendChild(messageElement.render());
    });
  }

  render() {
    let messagesWrapper = e('div', {className: 'messages'}, [
      this.messages
    ]);

    this.loadMessages(messagesWrapper);

    return messagesWrapper;
  }
}


