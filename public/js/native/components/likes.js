import { e } from '../element.js';

export default class Likes {
  constructor(message, user) {
    this.message = message;
    this.user = user;
    this.isOwnMessage = (this.message.user.id === this.user.id);
  }

  render() {
    return e('div', {
      className: `likes${this.isOwnMessage ? ' likes-self' : ''}`,
      on: {
        click: () => {
          if (this.isOwnMessage === false) {
            console.log('clicked');
          }
        }
      }
    }, this.message.likes.toString());
  }
}
