import Messages from './components/messages.js';
import Users from './components/users.js';

let currentUser = {id: 3};
let app = document.getElementById('app');
let messages = new Messages(currentUser);
let users = new Users(currentUser);

app.appendChild(users.render());
app.appendChild(messages.render());
