import Message from './message.js';

function getMessages() {
  return fetch('http://www.mocky.io/v2/5af9bbc32e00006000278d65')
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      return data.messages;
    });
}

export default Vue.component('messages', {
  props: [
    'currentUser'
  ],
  render: function (e) {
    let messages = this.messages.map((message) => {
      return e(Message, {props: {message: message, currentUser: this.currentUser}});
    });

    let dummyInput = e(
      'input',
      {
        attrs: {
          type: 'text',
        },
        on: {
          input: function (event) {
            console.log(event.target.value);
          }
        }
      }
    );

    return e('div', {class: 'messages'}, [
      messages,
      // dummyInput
    ]);
  },
  data: function () {
    return {
      messages: []
    }
  },
  created: async function () {
    this.messages = await getMessages();
  }
});
