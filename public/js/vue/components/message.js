import Likes from './likes.js';

export default Vue.component('message', {
  props: [
    'message',
    'user',
    'currentUser'
  ],
  render: function (e) {
    return e('div', {class: `message user-${this.message.user.id}`},
      [
        this.message.photo ? e('img', {
          class: 'photo',
          attrs: {
            src: this.message.photo
          }
        }) : null,
        this.message.body ? e('div', {class: 'body'},
          this.message.body
        ) : null,
        e('div', {class: 'details'},
          [
            e('div', {class: 'user'}, this.message.user.name),
            e('div', {class: 'created-at'}, this.message.createdAt),
            e(Likes, {props: {message: this.message, currentUser: this.currentUser}}),
          ]
        ),
      ]
    );
  },
});
