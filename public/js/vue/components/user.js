export default Vue.component('user', {
  props: [
    'user',
    'currentUser'
  ],
  render: function (e) {
    return e('div', {
        class: `user user-${this.user.id}${this.user.isActive ? ' is-active' : ''}`
      },
      [
        e('div', {
          class: 'heading',
          on: {
            click: () => {
              this.user.isActive = !this.user.isActive;
            }
          }
        }, [
          e('div', {class: 'name'}, this.user.name),
          e('div', {class: 'birthday'}, this.user.birthday),
        ]),
        e('div', {class: 'details'},
          [
            this.user.address ? e('dl', {}, [
              e('dt', {}, 'Address'),
              e('dd', {}, this.user.address),
            ]) : null,
            this.user.email ? e('dl', {}, [
              e('dt', {}, 'Email'),
              e('dd', {}, [
                e('a', {attrs: {href: `mailto:${this.user.email}`}}, this.user.email)
              ]),
            ]) : null,
            this.user.mobile ? e('dl', {}, [
              e('dt', {}, 'Mobile'),
              e('dd', {}, [
                e('a', {attrs: {href: `tel:${this.user.mobile}`}}, this.user.mobile)
              ]),
            ]) : null,
            this.currentUser.id === this.user.id ? e('a', {class: 'edit-details'}, 'Edit my details') : null,
          ]
        ),
      ]
    );
  },
});
