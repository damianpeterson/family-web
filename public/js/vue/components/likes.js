export default Vue.component('likes', {
  props: [
    'message',
    'currentUser'
  ],
  render: function (e) {
    let isOwnMessage = this.message.user.id === this.currentUser.id;

    return e('div', {
        class: `likes${isOwnMessage ? ' likes-self' : ''}`,
        on: {
          click: function (event) {
            if (isOwnMessage === false) {
              console.log('clicked');
            }
          }
        }
      },
      this.message.likes
    );
  },
});
