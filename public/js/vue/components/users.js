import User from './user.js';

function getUsers() {
  return fetch('http://www.mocky.io/v2/5b0e486e3200006d00c1995b')
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      data.users.forEach((user) => {
        user.isActive = false;
      });
      return data.users;
    });
}

export default Vue.component('users', {
  props: [
    'currentUser'
  ],
  render: function (e) {
    let users = this.users.map((user) => {
      return e(User, {props: {user: user, currentUser: this.currentUser}});
    });

    return e('div', {class: 'users'}, [
      users,
    ]);
  },
  data: function () {
    return {
      users: []
    }
  },
  created: async function () {
    this.users = await getUsers();
  }
});
