import Messages from './components/messages.js';
import Users from './components/users.js';

let vue = new Vue({
  el: '#app',
  data: {
    currentUser: {
      id: 3
    }
  },
  render(e) {
    return e('div', {class: 'app'}, [
      e(Users, {
        props: {
          currentUser: this.currentUser,
        }
      }),
      e(Messages, {
        props: {
          currentUser: this.currentUser,
          onChange: function (value) {
            console.log(value);
          }
        }
      }),
    ]);
  }
});

